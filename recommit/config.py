class Config:
    """
    Primary controls for the application.
    
    :ivar roots:                Project control. Where to find projects
    :ivar git_hosts:            Host control. Git host mapping. A dict of names to URLs with * representing the project name.
    :ivar pypi_hosts:           Host control. PYPI host mapping. List.
    :ivar web_hosts:            Host control. Web host mapping. A dict of names to local folders.
    :ivar doc_files:            Copied to web folder.
    :ivar doc_file_types:       When copying `doc_files` folders, only files of these types are copied.
    """
    INSTANCE: "Config" = None
    
    
    def __init__( self ):
        self.roots = ["/Users/martinrusilowicz/work/apps"]
        self.git_hosts = { "bitbucket"          : "https://bitbucket.org/mjr129/*.git",
                           "github"             : "https://github.com/mjr129/*.git",
                           "mcinerneylab-github": "https://github.com/JMcInerneyLab/*.git" }
        self.pypi_hosts = ["pypi"]
        self.web_hosts = { "web": "/Users/martinrusilowicz/work/web/software" }
        self.doc_files = "readme.md", "readme.rst", "icon.svg", "docs"
        self.doc_file_types = ".md", ".rst", ".svg", ".png", ".jpg"


Config.INSTANCE = Config()
