"""
Main entry point.
"""

import os
import os.path
import sys
from typing import Optional, Iterable, List

from recommit import Project, Config

from mhelper import ansi
from . import queries as Q


def print_box( mod: Optional[Iterable[Project]], all: Iterable[Project] ):
    for project in all:
        ver = Q.version.query( project )
        
        if mod is None:
            prefix = ""
        elif project in mod:
            prefix = "[X] "
        else:
            prefix = "[ ] "
        
        ver = ".".join( str( x ) for x in ver ).ljust( 10 )
        name = project.name.ljust( 25 )
        print( ansi.BACK_BRIGHT_BLACK + ansi.FORE_WHITE + "{}{}{}".format( prefix, name, ver ) + ansi.RESET )


def print_title( title ):
    title = "=" * 25 + " " + title + " " + "=" * 25
    print( ansi.BACK_BLUE + ansi.FORE_BRIGHT_WHITE + "=" * len( title ) + ansi.RESET )
    print( ansi.BACK_BLUE + ansi.FORE_BRIGHT_WHITE + title + ansi.RESET )
    print( ansi.BACK_BLUE + ansi.FORE_BRIGHT_WHITE + "=" * len( title ) + ansi.RESET )


def binput( message, default = True ):
    if Args.INSTANCE.default:
        print( "🔠 " + message + " <-- " + str( default ) )
        return default
    
    in_ = input( "🔠 " + message + " " )
    
    if in_ == "y":
        return True
    elif in_ == "n":
        return False
    else:
        return default


class Args:
    """
    recommit <args>
    
    Commits the project (defined by the current working directory).
    
    User arguments can be specified as `+argument(s)` to set or `-argument(s)` to unset.
    
    :ivar submit:        Pseudo-argument meaning "+add +commit +push +ver +pypi +docs"
    :ivar default:       Don't ask yes/no questions, just assume the default
    :ivar add:           Action: (external) git add
    :ivar commit:        Action: (external) git commit
    :ivar remote:        Action: (external) git remote add
    :ivar push:          Action: (external) git push
    :ivar pypi:          Action: (external) pypiupload
    :ivar inc:           Action: (external) updateversion
    :ivar incall:        Don't ask what version to increment, assume all (+incall) or none (-incall)
    :ivar docs:          Action: (internal) copy_documentation then make_documentation 
    :ivar index:         Action: (internal) make_index. Assumed for any modified web pages if `docs` or `all_docs` is set.
    :ivar query:         Show the dependencies.
    """
    INSTANCE: "Args" = None
    
    
    def __init__( self ):
        self.default = False
        self.add = False
        self.commit = False
        self.remote = False
        self.push = False
        self.pypi = False
        self.inc = False
        self.incall: Optional[bool] = None
        self.docs = False
        self.index = False
        self.query = False
        self.submit = None  # ignored


Args.INSTANCE = Args()


# noinspection SpellCheckingInspection
def main():
    orig_dir = os.getcwd()
    target = os.path.split( os.path.expanduser( orig_dir ) )[1]
    requires: List[Project] = __get_dependencies( target )
    args = Args.INSTANCE
    
    if len( sys.argv ) == 1:
        print_help()
        return
    
    for arg in sys.argv[1:]:
        if "?" in arg or "help" in arg or arg == "-h" or arg == "/h":
            print_help()
            return
        
        if arg[0] == "-":
            v = False
        elif arg[0] == "+":
            v = True
        else:
            raise ValueError( "Invalid argument: {}".format( arg ) )
        
        arg = arg[1:]
        
        if arg == "submit":
            arg = "add,commit,push,ver,pypi,docs"
        
        for a in arg.split( "," ):
            if not hasattr( args, a ):
                raise ValueError( "Invalid variable: {}".format( a ) )
            
            setattr( args, a, v )
    
    print_title( "arguments summary" )
    __print_object( args )
    
    if args.query:
        print_title( "query result" )
        
        for r in requires:
            __print_object( r )
    
    #
    # Check all
    #
    for project in requires:
        if project.setup_file:
            Q.setup.check_packages( project.setup_file )
    
    #
    # Git add all
    #
    if args.add:
        print_title( "all (git add)" )
        print_box( None, requires )
        
        if binput( "Action: git add all ({}/{})?".format( len( requires ), len( requires ) ) ):
            for project in requires:
                Q.git.add_all( project.path )
    
    #
    # Query version
    #
    if args.incall:
        args.inc = True
    
    if args.inc:
        if args.incall is None:
            i = input( "🔠 Query: Version increment based on '[c]commit' status or assume [a]ll or [n]one? " ).lower()
            
            if i == "a":
                args.incall = True
            elif i == "n":
                args.inc = False
            elif i == "c":
                args.incall = False
            else:
                print( "invalid option - exiting" )
                return 1
        
        if args.inc:
            if args.incall:
                to_increment = requires
            else:
                to_increment = set( x for x in requires if Q.git.query_commit_required( x.path ) )
            
            #
            # Update version
            #
            print_title( "to_increment" )
            print_box( to_increment, requires )
            
            if to_increment:
                if binput( "Enter to increment versions{} ({}/{})?".format( " (and git readd)" if args.add else "", len( to_increment ), len( requires ) ) ):
                    for project in to_increment:
                        Q.version.increment( project.path )
                        
                        if args.add:
                            Q.git.add_all( project.path )
            else:
                print_nothing_to_do()
    
    #
    # Git commit
    #
    if args.commit:
        to_commit = set( x for x in requires if Q.git.query_commit_required( x.path ) )
        print_title( "to_commit" )
        print_box( to_commit, requires )
        
        if to_commit:
            if binput( "Action: git commit ({}/{})?".format( len( to_commit ), len( requires ) ) ):
                for project in to_commit:
                    Q.git.commit( project.path )
        else:
            print_nothing_to_do()
    
    #
    # Query needs remotes
    #
    if args.remote:
        print_title( "all (query-needs-remotes)" )
        print_box( None, requires )
        
        if binput( "Query: [y]es, add remotes remotes based on query status or assume [n]one?" ):
            needs_remotes = __get_needs_remotes( requires )
        else:
            needs_remotes = []
        
        # Git add remotes
        print_title( "needs_remotes" )
        print_box( [x[0] for x in needs_remotes], requires )
        if needs_remotes and binput( "Enter to add remotes ({}/{})?".format( len( needs_remotes ), len( requires ) ) ):
            for project, host, url in needs_remotes:
                Q.git.add_remote( project.path, host, url )
    
    #
    # Git push
    #
    if args.push:
        print_title( "all (git-push)" )
        print_box( None, requires )
        
        if binput( "Action: git push ({}/{})?".format( len( requires ), len( requires ) ) ):
            for project in requires:
                for remote in Q.git.query_remotes( project.path ).values():
                    Q.git.push( project.path, remote )
    
    #
    # Upload to Pypi
    #
    if args.pypi:
        print_title( "needs_pypi" )
        needs_pypi = __get_needs_pypi( requires )
        print_box( needs_pypi, requires )
        
        if needs_pypi:
            if binput( "Action: upload to Pypi ({}/{})?".format( len( needs_pypi ), len( requires ) ) ):
                for project in needs_pypi:
                    Q.pypi.upload( project.path )
        else:
            print_nothing_to_do()
    
    #
    # Copy documentation
    #
    modified_web_dests = ()
    
    if args.docs:
        print_title( "all (make-docs)" )
        print_box( None, requires )
        
        if binput( "Action: make documentation ({}/{})?".format( len( requires ), len( requires ) ) ):
            modified_web_dests = set()
            
            for project in requires:
                for host, dest in Config.INSTANCE.web_hosts.items():
                    if host in project.hosts:
                        Q.documentation.copy_documentation( project, dest )
                        modified_web_dests.add( dest )
    
    if args.index:
        modified_web_dests = Config.INSTANCE.web_hosts.values()
    
    for modified_web_dest in modified_web_dests:
        if binput( "Action: make indices ({} folders)?".format( len( modified_web_dests ) ) ):
            Q.documentation.make_index( modified_web_dest )
    
    # Complete
    os.chdir( orig_dir )
    print( "COMPLETE." )


def print_nothing_to_do():
    print( ansi.BACK_GREEN + ansi.FORE_WHITE + "Nothing to do." + ansi.RESET )


def print_help():
    print( "recommit" )
    print( "========" )
    print( "" )
    print( "usage" )
    print( "-----" )
    print( str( Args.__doc__ ).replace( ":ivar ", "+" ) )


def __print_object( r ):
    for k, v in r.__dict__.items():
        if k.startswith( "_" ):
            continue
        
        print( "{} = {}".format( k, v ) )


def __get_needs_pypi( requires ):
    needs_pypi = []
    
    for project in requires:
        if any( x in project.hosts for x in Config.INSTANCE.pypi_hosts ):
            r = Q.pypi.query_remote_version( project.path )
            l = Q.version.query( project )
            print( "{} REMOTE {} LOCAL {}".format( project, r, l ) )
            
            if r != l:
                needs_pypi.append( project )
    
    return needs_pypi


def __get_needs_remotes( requires ):
    needs_remotes = []
    for project in requires:
        remotes = Q.git.query_remotes( project.path )
        
        for host in project.hosts:
            url = Config.INSTANCE.git_hosts.get( host )
            
            if url is None:
                continue
            
            url = url.replace( "*", project.name )
            
            if url in remotes:
                needs_remotes.append( (project, host, url) )
    return needs_remotes


def __get_dependencies( target ) -> List[Project]:
    dependencies: List[Project] = []
    to_process = [target]
    processed = { target }
    
    while to_process:
        name = to_process.pop()
        
        try:
            dir = Q.multidir.query( name )
        except FileNotFoundError:
            continue
        
        project = Q.meta.query( dir )
        dependencies.append( project )
        
        for name_2 in project.dependencies:
            if name_2 in processed:
                continue
            
            processed.add( name_2 )
            to_process.append( name_2 )
    
    return dependencies
