"""
Queries the meta-data in _readme.md_.
"""
from typing import List


LS = List[str]


class Project:
    """
    Defines a project.
    """
    
    
    def __init__( self ) -> None:
        self.path: str = None
        self.title: str = None
        self.short_description = None
        self.hosts: LS = []
        self.authors: LS = []
        self.languages: LS = []
        self.keywords: LS = []
        self.types: LS = []
        self.license: LS = []
        self.ini_file: str = None
        self.readme_file: str = None
        self.name: str = None
        self.setup_file: str = None
        self.dependencies: LS = []
        self.version: str = None
        
    def __str__(self):
        return self.name
