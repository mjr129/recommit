"""
Git related functions.
"""

import os
from tempfile import NamedTemporaryFile
from typing import Dict, Tuple
from mhelper.subprocess_helper import execute


def query_remotes( path: str ) -> Dict[str, str]:
    """
    For a `path`, gets a dictionary of remote names to URLs.
    """
    remotes = [x for x in execute( ["git", "remote"], dir = path, rx_out = True ).split( "\n" ) if x]
    remote_urls = { }
    
    for remote in remotes:
        remote_url = execute( ["git", "remote", "get-url", remote], dir = path, rx_out = True ).strip()
        remote_urls[remote_url] = remote
    
    return remote_urls


def query_commit_required( path: str ) -> bool:
    """
    For a `path`, returns if a `git commit` is required.
    """
    result = execute( ["git", "status"], dir = path, rx_out = True )
    
    if "Changes to be committed" in result or "Changes not staged for commit" in result or "Untracked files" in result:
        return True
    
    return False


def query_pull_push_required( path: str, remote: str ) -> Tuple[bool, bool]:
    """
    For a `path`, returns a tuple of:
        * If a pull is required
        * If a push is required
    """
    execute( ["git", "remote", "update", remote], dir = path )
    my_commits = set( x[7:] for x in execute( ["git", "log", "master"], dir = path ).split( "\n" ) if x.startswith( "commit " ) )
    remote_commits = set( x[7:] for x in execute( ["git", "log", "{}/master".format( remote )], dir = path ).split( "\n" ) if x.startswith( "commit " ) )
    
    needs_pull = remote_commits - my_commits
    needs_push = my_commits - remote_commits
    
    return bool( needs_pull ), bool( needs_push )


def add_remote( path: str, name: str, url: str ) -> None:
    """
    Adds a new remote to a `path` with the given `name` and `url`.
    """
    execute( ["git", "remote", "add", name, url], dir = path )
    execute( ["git", "remote", "update", name], dir = path )


def add_all( path: str ) -> None:
    """
    Performs a `git add --all`.
    """
    execute( ["git", "add", "--all"], dir = path )


def commit( path: str ) -> None:
    """
    Performs a `git commit`
    """
    os.chdir( path )
    i = 0
    
    execute( ["git", "diff", "--staged"], dir = path )
    
    name = os.path.split( path )[1]
    print( "{} - Enter git commit message. Terminate `/` to start a new line or `/x` to cancel.".format( name ) )
    
    with NamedTemporaryFile( "w" ) as file:
        while True:
            if i != 0:
                file.write( "\n" )
            
            i += 1
            l = input( "{}:".format( i ) )
            
            if l.endswith( "/" ):
                file.write( l[:-1] )
            elif l.endswith( "/x" ):
                print( "CANCELLED" )
                return
            else:
                file.write( l )
                break
        
        file.flush()
        
        execute( ["git", "commit", "--file", file.name], dir = path )


def push( path: str, remote: str ) -> None:
    """
    Performs a `git push` to the specified `remote`
    """
    execute( ["git", "push", remote], dir = path )


def pull( path: str, remote: str ) -> None:
    """
    Performs a `git push` from the specified `remote`
    """
    execute( ["git", "pull", remote], dir = path )
