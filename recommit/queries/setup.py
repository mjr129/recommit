"""
Queries _setup.py_
"""

import os.path

from mhelper import file_helper


class _FakeSetup:
    def __init__( self ):
        pass
    
    
    def __call__( self, *args, **kwargs ):
        self.args = args
        self.kwargs = kwargs


def query_key( file_name: str, key: str, default: object ):
    """
    Queries a `key` in "setup.py".
    """
    fs = query_setup( file_name )
    install_requires = fs.kwargs.get( key, default )
    return install_requires


def query_setup( file_name: str ):
    import distutils.core as DUC
    fs = _FakeSetup()
    DUC.setup = fs
    import importlib.util
    spec = importlib.util.spec_from_file_location( "fake.name", file_name )
    foo = importlib.util.module_from_spec( spec )
    spec.loader.exec_module( foo )
    return fs


def check_packages( file_name: str ):
    listed = query_key( file_name, "packages", [] )
    
    root = os.path.split( file_name )[0]
    xx = len( root ) + 1
    scanned = [x[xx:].replace( os.path.sep, "." ) for x in list_subdirs( root ) if is_package( x )]
    
    to_warn = []
    
    for scan in scanned:
        if scan not in listed:
            if scan != "docs":
                to_warn.append( "The folder '{}' is not present in setup.py::packages.".format( scan ) )
    
    for list in listed:
        if list not in scanned:
            to_warn.append( "The setup.py::packages item does not have a corresponding folder: {}".format( list ) )
    
    if to_warn:
        print( "{} WARNINGS FOR {}".format( len( to_warn ), file_name ) )
        print( "\n".join( "WARNING: {}".format( x ) for x in to_warn ) )
        raise ValueError( "One or more setup.py warnings were issued." )


def list_subdirs( root ):
    for a, b, c in os.walk( root ):
        if a != root:
            yield a


def is_package( dir_name: str ):
    if not os.path.isdir( dir_name ):
        return False
    
    if not file_helper.list_dir( dir_name, recurse = True, filter = ".py" ):
        return False
    
    return True
