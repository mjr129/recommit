import shutil
import os
import os.path
from sys import path

from recommit import Config
from recommit.project import Project
from recommit.queries import meta
from mhelper import file_helper


def copy_documentation( project: Project, web_dir: str ) -> None:
    """
    Copies the documentation (`.md`) and meta-data (`.json`) to the web folder and automatically transforms
    it into HTML using `make_documentation`.
    """
    dir_out = os.path.join( web_dir, project.name )
    
    # Delete existing
    if os.path.isdir( dir_out ):
        shutil.rmtree( dir_out )
    
    os.mkdir( dir_out )
    
    # These files and folders get copied to the web folder
    for search in Config.INSTANCE.doc_files:
        search_fn = os.path.join( project.path, search )
        
        if os.path.isfile( search_fn ):
            files = [(project.path, search_fn)]
        elif os.path.isdir( search_fn ):
            files = [(search_fn, x) for x in file_helper.list_dir( search_fn )]
        else:
            continue
        
        for rt_, in_ in files:
            out_ = os.path.join( dir_out, in_[len( rt_ ) + 1:] )
            
            if os.path.isfile( out_ ):
                raise FileExistsError( "Already exists '{}'".format( out_ ) )
            
            dout_ = file_helper.get_directory( out_ )
            file_helper.create_directory( dout_ )
            shutil.copy( in_, out_ )
    
    # Also copy the meta-data, this is for `make_index` below; it is not used by `webmaker`
    json_file_out = os.path.join( dir_out, ".reporg.json" )
    meta.save( project, json_file_out )


def fn_cp( s, d ):
    ext = file_helper.get_extension( s )
    
    if ext not in Config.INSTANCE.doc_file_types:
        return
    
    shutil.copy2( s, d )


def __get_icons( project: Project ) -> str:
    r = []
    
    r.extend( set( "host_" + x.replace( "-", "_" ) for x in project.hosts ) )
    
    if "application-gui" in project.types or "gui" in project.types:
        r.append( "type_gui" )
    
    if "application-cli" in project.types or "cli" in project.types:
        r.append( "type_cli" )
    
    if "library" in project.types or "package" in project.types:
        r.append( "type_lib" )
    
    if "python" in project.languages or "python3" in project.languages:
        r.append( "lang_py" )
    
    if "c++" in project.languages or "c" in project.languages or "cpp" in project.languages:
        r.append( "lang_cpp" )
    
    if "c#" in project.languages or "cs" in project.languages or "csharp" in project.languages:
        r.append( "lang_cs" )
    
    if "text" in project.languages or "latex" in project.languages:
        r.append( "lang_tex" )
    
    if "doc" in project.languages or "word" in project.languages:
        r.append( "lang_doc" )
    
    if "text" in project.languages or "txt" in project.languages:
        r.append( "lang_txt" )
    
    if "file" in project.languages or "files" in project.languages:
        r.append( "lang_dir" )
    
    return " ".join( r )


def make_index( directory_name: str ) -> None:
    """
    Creates an `index.rst` listing all `.json" files in a directory.
    
    :param directory_name: Directory to process
    """
    r = []
    r.append( "Software index" )
    r.append( "--------------" )
    r.append( "" )
    
    for subdir in file_helper.list_sub_dirs( directory_name ):
        json_file = os.path.join( subdir, ".reporg.json" )
        
        if os.path.isfile( json_file ):
            project = meta.load( json_file )
            
            r.append( ".. sw_card:: {}".format( project.name ) )
            r.append( "    :icons: {}".format( __get_icons( project ) ) )
            r.append( "    :description: {}".format( project.short_description ) )
            r.append( "    :url: {}/index.html".format( file_helper.get_filename( subdir ) ) )
            r.append( "" )
    
    fno = os.path.join( directory_name, "index.rst" )
    file_helper.write_all_text( fno, r )
