"""
Queries _Pypi_
"""

import os.path
import json
import urllib.request

from typing import Tuple

from mhelper import subprocess_helper as master


def upload( path: str ) -> None:
    """
    Uploads to Pypi using "pypiupload"
    """
    master.run( path, "python -m pypiupload" )


def query_remote_version( path: str ) -> Tuple[int, ...]:
    """
    Obtains the version stored on Pypi.
    """
    name = os.path.split( path )[1]
    url = "https://pypi.org/pypi/{}/json".format( name )
    data = urllib.request.urlopen( url ).read().decode( "utf-8" )
    jso = json.loads( data )
    version_text = jso["info"]["version"]
    computer_version = tuple( int( x ) for x in version_text.split( "." ) )
    return computer_version
