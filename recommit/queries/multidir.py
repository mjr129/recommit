import os.path

from mhelper import NOT_PROVIDED

from recommit import Config


def query( name: str, default=NOT_PROVIDED ) -> str:
    for root in Config.INSTANCE.roots:
        c = os.path.join( root, name )
        if os.path.isdir( c ):
            return c

    if default is NOT_PROVIDED:    
        raise FileNotFoundError( "Directory '{}' not found in any of {}".format( name, Config.INSTANCE.roots ) )
    
    return default
