import re
import os.path

import jsonpickle

from recommit.project import Project
from . import setup
from mhelper import file_helper, coalesce


__README_META = re.compile( "meta[^`]*```[a-z]*\n([^`]+)```", re.MULTILINE | re.IGNORECASE | re.DOTALL )


def save( project: Project, json_file: str ) -> None:
    json = jsonpickle.dumps( project )
    file_helper.write_all_text( json_file, json )


def load( json_file: str ) -> Project:
    json = file_helper.read_all_text( json_file )
    r = jsonpickle.loads( json )
    assert isinstance( r, Project )
    return r


def query( path: str ) -> Project:
    p = Project()
    p.path = path
    p.name = os.path.split( path )[1]
    assert "#" not in p.name, p.name
    update( p )
    return p


def update( p ):
    try:
        p.setup_file = __if_exists( os.path.join( p.path, "setup.py" ) )
        p.readme_file = __if_exists( os.path.join( p.path, "readme.md" ) ) or __if_exists( os.path.join( p.path, "readme.rst" ) )
        p.ini_file = __if_exists( os.path.join( p.path, "meta.ini" ) ) or __if_exists( os.path.join( p.path, "docs", "meta.ini" ) )
        __interpret( p, __parse_readme( p.readme_file ) )
        __interpret( p, __parse_ini( p.ini_file ) )
        __interpret( p, coalesce( __parse_setup( p.setup_file ), "kwargs" ) )
    except Exception as ex:
        raise ValueError( "Failed to query «{}»".format( p.path ) ) from ex


def __pull( d, *vv ):
    for v in vv:
        r = d.get( v, None )
        
        if r is None:
            pass
        elif isinstance( r, str ):
            yield r
        elif isinstance( r, list ) or isinstance( r, tuple ):
            yield from r
        else:
            raise ValueError( "Unrecognised type: {}".format( type( r ) ) )


def __interpret( project: Project, data: dict ):
    if data is None:
        return
    
    if "title" in data:
        project.title = data["title"]
        assert "#" not in project.title, project.title
    
    if "short_description" in data:
        project.short_description = data["short_description"]
        
        project.short_description = re.sub( r"\(.*\)", "", project.short_description )
        project.short_description = project.short_description.replace( "[", "" ).replace( "]", "" )
    
    if "version" in data:
        project.version = data["version"]
    
    project.hosts.extend( x.lower() for x in __pull( data, "host", "hosts" ) )
    project.authors.extend( __pull( data, "author", "authors" ) )
    project.languages.extend( __pull( data, "language", "languages" ) )
    project.keywords.extend( __pull( data, "keywords", "tags" ) )
    project.types.extend( __pull( data, "type", "types", "interface", "interfaces" ) )
    project.license.extend( __pull( data, "license" ) )
    project.dependencies.extend( __pull( data, "install_requires" ) )


def __if_exists( file_name: str ):
    if os.path.isfile( file_name ):
        return file_name
    else:
        return None


def __parse_ini_text( text: str ):
    r = { }
    for line in text.split( "\n" ):
        b, s, a = line.partition( "=" )
        
        if s:
            if "," in a:
                a = [x.strip() for x in a.split( "," )]
            else:
                a = a.strip()
            
            r[b.strip()] = a
    
    return r


def __parse_ini( file_name: str ):
    if not file_name or not os.path.isfile( file_name ):
        return { }
    
    txt = file_helper.read_all_text( file_name )
    return __parse_ini_text( txt )


def __parse_readme( file_name: str ):
    if not file_name or not os.path.isfile( file_name ):
        return { }
    
    txt = file_helper.read_all_text( file_name )
    rxs = __README_META.search( txt )
    
    
    
    title = None
    first = None
    
    for line in txt.split( "\n" ):
        line = line.strip( " -=" )
        
        if line:
            if title is None:
                title = line.strip( " #\t" )
            elif first is None:
                first = line
                break
    
    
    if first is None:
        raise ValueError( "Failed to get first line from {}".format( repr( file_name ) ) )
    
    r = { "title"            : title.strip(),
          "short_description": first }
    
    
    if rxs:
        result = rxs.group( 1 )
        r.update( __parse_ini_text( result ) )
        
    return r


def __parse_setup( file_name: str ):
    if not file_name:
        return None
    
    return setup.query_setup( file_name )
