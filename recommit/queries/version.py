from typing import Tuple
from recommit import Project
from mhelper import subprocess_helper


def query( project: Project ) -> Tuple[int, ...]:
    """
    Obtains the version form "setup.py".
    """
    from . import meta
    meta.update( project )
    
    if project.version is None:
        raise ValueError( "Project has no version: {}".format( project.name ) )
    
    computer_version = tuple( int( x ) for x in project.version.split( "." ) )
    return computer_version


def increment( path: str ) -> None:
    """
    Increments the version number using `updateversion`.
    """
    subprocess_helper.run_subprocess( path, "updateversion" )
