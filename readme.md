Recommit
========

CLI utility to iterate a Python project's dependencies (on disk), update their version numbers, commit them to Git, push them to the Git servers, upload them to Pypi and update their webpage.


Installation
------------

Install in source code mode:

```bash
cd /path/to/recommit
sudo pip install -e .
```


Usage
-----

`main.py` must be modified to suit the user's own preferences and directory structure.
Alternatively, the `recommit.queries` library may be imported and manipulated from Python.

`recommit` is bound to `recommit.main.main()`.
Call this from the Python project folder.
All dependencies are assumed to be siblings of this folder.

```bash
recommit
```



Meta
----

```ini
host=bitbucket
version=0.0.0.2
language=python
interface=cli,library
author=Martin Rusilowicz
created=2018-06-18
license=AGPLv3
```
