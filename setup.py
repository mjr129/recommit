from distutils.core import setup


setup( name = "recommit",
       url = "https://bitbucket.org/mjr129/recommit",
       version = "0.0.0.2",
       description = "Commit a repository and all its dependencies.",
       author = "Martin Rusilowicz",
       license = "https://www.gnu.org/licenses/agpl-3.0.html",
       packages = ["recommit"],
       entry_points = { "console_scripts": ["recommit = recommit.main:main"] },
       install_requires = ["mhelper"],
       python_requires = ">=3.6"
       )
